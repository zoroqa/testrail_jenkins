"""
TestRail integration
"""
from xml.dom.minidom import parse  # to parse the report.xml
import os  # to find the file and write back
import shutil  # shutil.move is used to move the file after it is done processing
from os import listdir  # to get all the xml files in the given directory
import sys  # to pass the runID as arguments
import testrail  # Api given by Testrail to read and write test results
import xml.dom.minidom
import dict_digger  # $ pip install dict_digger #


# Global variables for Test_rail Url and login credentials
TESTRAIL_URL = 'https://zoro.testrail.net/'
TESTRAIL_USER = 'manoj.mukka@zoro.com'
TESTRAIL_PASSWORD = 'zorothefox'
# project name in the Testrail
PROJECT_NAME = 'Zoro.com'  # should be capital 'Z'


# Creating a  Testrail object for the class APIClient form testrail.py and passing login credentials
def create_testrail_object():
    tr_object = testrail.APIClient(TESTRAIL_URL)
    tr_object.user = TESTRAIL_USER
    tr_object.password = TESTRAIL_PASSWORD
    return tr_object


# Function to get the project ID using the project name with the help of send_get
def get_project_id(tr_object):
    project_id = None
    projects = tr_object.send_get('get_projects')
    for project in projects:
        if project['name'] == PROJECT_NAME:
            project_id = project['id']  # accessing the project ID
            break
    return project_id


# Function to get the run IDs using the project id with the help of send_get
def test_run_ids(tr_object, project_id):
    run_ids = tr_object.send_get('get_runs/{}'.format(project_id))  # {}.format matches the data type of variables
    ids = []
    for i in run_ids:
        ids.append(i.get('id'))
    return ids


# Function to get the suite IDs using the project id with the help of send_get
def get_suite_ids(tr_object, project_id):
    suite_ids = tr_object.send_get('get_suites/{}'.format(project_id))
    ids = []
    for j in suite_ids:
        ids.append(j.get('id'))
    return ids


# Function to get the case IDs using the project id and suite id with the help of send_get
def get_case_ids(tr_object, project_id, suite_ids):
    a = {}
    for suite_id in suite_ids:
        case_ids = tr_object.send_get('get_cases/{}&suite_id={}'.format(project_id, suite_id))
        ids = []
        for k in case_ids:
            ids.append(k.get('id'))
        a[suite_id] = ids
    return a


def get_tests_id(tr_object, run_ids):
    test_details = {}
    for r in run_ids:
        id_test_cases = tr_object.send_get('get_tests/{}'.format(r))
        for case in id_test_cases:
            test_details[case.get('id')] = {
                'run_ids': case.get('run_id'),
                'title': case.get('title'),
                'T.id': case.get('id'),
                'case_id': case.get('case_id')
            }
    return test_details


def add_test_results(tr_object, result_case_id, status_id):
    if status_id == 1:
        status_flag = {'status_id': status_id,
                       'comment': "This test worked fine!"}
    if status_id == 5:
        status_flag = {'status_id': status_id,
                       'comment': "This test has a trace error"}
    tr_object.send_post('add_result_for_case/' + str(run_id) + '/' + str(result_case_id), status_flag)


def get_result_id(tr_object, test_ids, trace_list):
    for trace in trace_list:
        if trace.hasAttribute("name"):
            title = trace.getAttribute("name")
            title = title.split(':')[1].lstrip()
        if title != None:
            result_case_id = get_case_id(title, test_ids)
            if result_case_id != None:
                print(result_case_id)
            else:
                print("Test case not found")
                result_case_id = None

        if result_case_id != None:
            error = trace.getElementsByTagName("error")
            if error:
                status_id = 5
                print(status_id)
            else:
                status_id = 1
                print(status_id)
            if status_id != None:
                add_test_results(tr_object, result_case_id, status_id)


def main():
    tr_object = create_testrail_object()
    project_id = get_project_id(tr_object)
    run_ids = test_run_ids(tr_object, project_id)
    suite_ids = get_suite_ids(tr_object, project_id)
    case_ids = get_case_ids(tr_object, project_id, suite_ids)
    test_details = get_tests_id(tr_object, run_ids)
    report_path = "target/test-reports/"
    present_dir = os.getcwd()
    r_path = os.path.join(present_dir, report_path)
    for f in listdir(r_path):
        report_name = os.path.join(r_path, f)
        print(report_name)
        dom_tree = xml.dom.minidom.parse(os.path.join(r_path, f))
        collection = dom_tree.documentElement
        trace_list = collection.getElementsByTagName("testcase")
        get_result_id(tr_object, test_ids, trace_list)
    print(run_ids)
    print(suite_ids)
    print(case_ids)
    print(test_details)


if __name__ == "__main__": main()
