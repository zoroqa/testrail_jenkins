"""
TestRail integration
"""
# C:\Users\xmxm274\AppData\Local\Programs\Python\Python35-32\python.exe
# C:\Users\xmxm274\PycharmProjects\Testrail_Jenkins\testrail_jenkins_runresults.py %ID%

from xml.dom.minidom import parse  # to parse the report.xml
import os  # to find the file and write back
import shutil  # shutil.move is used to move the file after it is done processing
from os import listdir  # to get all the xml files in the given directory
import sys  # to pass the runID as arguments
import testrail  # Api given by Testrail to read and write test results
import xml.dom.minidom
import dict_digger  # $ pip install dict_digger #

TESTRAIL_URL = 'https://zoro.testrail.net/'
TESTRAIL_USER = 'manoj.mukka@zoro.com'
TESTRAIL_PASSWORD = 'zorothefox'
run_id = 9


# project name in the Testrail
# PROJECT_NAME = 'Zoro.com'  # should be capital 'Z'


# Creating a  Testrail object for the class APIClient form testrail.py and passing login credentials
def create_testrail_object():
    tr_object = testrail.APIClient(TESTRAIL_URL)
    tr_object.user = TESTRAIL_USER
    tr_object.password = TESTRAIL_PASSWORD
    return tr_object


# To get the case_ids and titles of each test case in the specified runID
def get_tests_id(tr_object, run_id):
    test_details = {}
    # using send_get to get the test case details
    id_test_cases = tr_object.send_get('get_tests/{}'.format(run_id))
    for case in id_test_cases:
        test_details[case.get('id')] = {
            'title': case.get('title'),
            'T.id': case.get('id'),
            'case_id': case.get('case_id')
        }
    return test_details


# To get the corresponding test case id using title
def get_case_id(title, test_ids):
    name = title
    test_dictionary = test_ids
    test_case_id = None
    for test_case in test_dictionary:
        case_title = dict_digger.dig(test_dictionary, test_case, 'title')
        if case_title == name:
            test_case_id = dict_digger.dig(test_dictionary, test_case, 'case_id')
    return test_case_id


def main():
    tr_object = create_testrail_object()
    test_ids = get_tests_id(tr_object, run_id)
    report_path = "target/test-reports/"
    present_dir = os.getcwd()
    r_path = os.path.join(present_dir, report_path)
    for f in listdir(r_path):
        report_name = os.path.join(r_path, f)
        print(report_name)
        dom_tree = xml.dom.minidom.parse(os.path.join(r_path, f))
        collection = dom_tree.documentElement
        trace_list = collection.getElementsByTagName("testcase")
        get_result_id(tr_object, test_ids, trace_list)


def get_result_id(tr_object, test_ids, trace_list):
    for trace in trace_list:
        if trace.hasAttribute("name"):
            title = trace.getAttribute("name")
            title = title.split(':')[1].lstrip()
        if title != None:
            result_case_id = get_case_id(title, test_ids)
            if result_case_id != None:
                print(result_case_id)
            else:
                print("Test case not found")
                result_case_id = None

        if result_case_id != None:
            error = trace.getElementsByTagName("error")
            if error:
                status_id = 5
                print(status_id)
            else:
                status_id = 1
                print(status_id)
            if status_id != None:
                add_test_results(tr_object, result_case_id, status_id)


if __name__ == "__main__": main()
